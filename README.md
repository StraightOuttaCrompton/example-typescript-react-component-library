# Example typescript react component library

[![NPM](https://img.shields.io/npm/v/example-typescript-react-component-library.svg)](https://www.npmjs.com/package/example-typescript-react-component-library) [![LICENSE](https://img.shields.io/npm/l/example-typescript-react-component-library.svg?color=green)](https://www.npmjs.com/package/example-typescript-react-component-library)

<!--
[![coverage report](https://gitlab.com/StraightOuttaCrompton/example-typescript-react-component-library/badges/master/coverage.svg)](https://gitlab.com/StraightOuttaCrompton/example-typescript-react-component-library/commits/master) -->

An example project setup that allows for publishing a [TypeScript](https://github.com/Microsoft/TypeScript) [React](https://github.com/facebook/react) component module to [npm](https://www.npmjs.com/package/example-typescript-react-component-library).

Initially setup using [create-react-library](https://github.com/transitive-bullshit/create-react-library) and converted to use `TypeScript`.

## Project structure

The project contains a `src` directory containing the module files that get published to npm, and an `example` directory containing a working `create-react-app` project that contains an example usage of the module.

## CI

This example also includes a `.gitlab-ci.yml` which publishes the module to `npm` (based on [this guide](https://vgarcia.dev/blog/2019-11-06--publish-a-package-to-npm-step-by-step/)) and deploys an example build to [GitLab pages](https://straightouttacrompton.gitlab.io/example-typescript-react-component-library).

## Development

### Setup

#### Module

Whilst in the root directory, install dependencies

```sh
npm install
```

To run in watch mode

```sh
npm start
```

To build

```sh
npm run build
```

#### Example create-react-app

First, cd into the example directory

```sh
cd example
```

Install dependencies

```sh
npm install
```

To run development server

```sh
npm start
```

To build

```sh
npm run build
```

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md).

## Deployed Module

### Install

```sh
npm install --save example-typescript-react-component-library
```

### Usage

```tsx
import * as React from "react";

import MyComponent from "example-typescript-react-component-library";

class Example extends React.Component {
    render() {
        return <MyComponent />;
    }
}
```
