import React, { Component } from "react";

import ExampleComponent from "example-typescript-react-component-library";

export default class App extends Component {
    render() {
        return (
            <div>
                <ExampleComponent text="Modern React component module" />
            </div>
        );
    }
}
