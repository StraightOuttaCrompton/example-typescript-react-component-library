# 1.0.0 (2022-09-19)


### Bug Fixes

* trigger new release ([7356637](https://gitlab.com/StraightOuttaCrompton/example-typescript-react-component-library/commit/73566370fe9b8e18919285a4f12c31d59c9d9647))

## [1.0.3](https://gitlab.com/StraightOuttaCrompton/example-typescript-react-component-library/compare/v1.0.2...v1.0.3) (2020-10-24)

## [1.0.2](https://gitlab.com/StraightOuttaCrompton/example-typescript-react-component-library/compare/v1.0.1...v1.0.2) (2020-03-30)

## [1.0.1](https://gitlab.com/StraightOuttaCrompton/example-typescript-react-component-library/compare/v1.0.0...v1.0.1) (2020-03-30)

# 1.0.0 (2020-03-29)


### Bug Fixes

* trigger new release ([d2c0531](https://gitlab.com/StraightOuttaCrompton/example-typescript-react-component-library/commit/d2c053148a63c9091175a28ce6b16400b33279c8))
